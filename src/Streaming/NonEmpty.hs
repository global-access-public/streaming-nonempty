{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ImportQualifiedPost #-}

module Streaming.NonEmpty where

import Data.List.NonEmpty (NonEmpty (..))
import Streaming (Of ((:>)), Stream, wrap, first, effect, inspect)
import Streaming.Prelude qualified as S

-- | A stream with at least one functorial layer
newtype NEStream f m a = NEStream (f (Stream f m a)) deriving (Functor)

-- | Is a stream
toStream :: (Functor f, Monad m) => NEStream f m r -> Stream f m r
toStream (NEStream f) = wrap f

fromStream :: Monad m => Stream f m a -> m (Either a (NEStream f m a))
fromStream s = fmap NEStream <$> inspect s

map :: Monad m => (t -> b) -> NEStream (Of t) m a -> NEStream (Of b) m a
map f (NEStream (x :> s)) = NEStream (f x :> S.map f s)

-- | fold1 is a safe way to apply folds that require at least one element to
-- streams.
--
-- Can be used in combination with
-- Control.Foldl.NonEmpty.'Control.Foldl.NonEmpty.purely'
-- from the foldl package.
fold1
  :: Monad m
  => (a -> x) -> (x -> a -> x) -> (x -> b)
  -> NEStream (Of a) m r
  -> m (Of b r)
fold1 begin step done (NEStream (x :> s)) = S.fold step (begin x) done s

-- | Variant of 'fold1' that does not yield the return value of the stream.
fold1_
  :: Monad m
  => (a -> x) -> (x -> a -> x) -> (x -> b)
  -> NEStream (Of a) m r
  -> m b
fold1_ begin step done = fmap S.fst' . fold1 begin step done

-- | Variant of 'fold1' that works with an effectful fold.
foldM1
  :: Monad m
  => (a -> m x) -> (x -> a -> m x) -> (x -> m b)
  -> NEStream (Of a) m r
  -> m (Of b r)
foldM1 begin step done (NEStream (x :> s)) = S.foldM step (begin x) done s

-- | Variant of 'fold1_' that works with an effectful fold.
foldM1_
  :: Monad m
  => (a -> m x) -> (x -> a -> m x) -> (x -> m b)
  -> NEStream (Of a) m r
  -> m b
foldM1_ begin step done = fmap S.fst' . foldM1 begin step done

-- | harvest the list with the result
toNonEmpty :: Monad m => NEStream (Of a) m r -> m (Of (NonEmpty a) r)
toNonEmpty (NEStream (x :> r)) = first (x :|) <$> S.toList r

-- | harvest the list only
toNonEmpty_ :: Monad m => NEStream (Of a) m r -> m (NonEmpty a)
toNonEmpty_ = fmap S.fst' . toNonEmpty

-- | Group elements of a stream by some equality into non empty groups.
--
-- The concatenation of the result is equal to the input stream, i.e.
--
--   @concats . maps toStream . groupBy f = id@
--
-- Example:
--
-- >>> S.print . S.mapped toNonEmpty . groupBy (==) $ S.each "Mississippi"
-- 'M' :| ""
-- 'i' :| ""
-- 's' :| "s"
-- 'i' :| ""
-- 's' :| "s"
-- 'i' :| ""
-- 'p' :| "p"
-- 'i' :| ""
--
-- When a supplied relation is not transitive, it is important to remember that
-- equality is checked against the first element in the group, not against the
-- nearest neighbour:
--
-- >>> S.print . S.mapped toNonEmpty . groupBy (\a b -> b - a < 5) $ S.each [0..19]
-- 0 :| [1,2,3,4]
-- 5 :| [6,7,8,9]
-- 10 :| [11,12,13,14]
-- 15 :| [16,17,18,19]
groupBy
  :: (Monad m)
  => (a -> a -> Bool)
  -> Stream (Of a) m r
  -> Stream (NEStream (Of a) m) m r
groupBy equals = loop
  where
    loop stream = effect $ do
      e <- S.next stream
      pure $ case e of
        Left r -> pure r
        Right (a, p') ->
          wrap $
            fmap loop (NEStream (a :> S.span (equals a) p'))
