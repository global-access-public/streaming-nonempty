# streaming-nonempty

A correct _groupBy_ function is expected to produce only non-empty groups, which means we should be able to fold them up with only Semigroup constraint on the values. This is not the case for `Data.List.groupBy` as well for the Streaming lib counterpart.

This package export a _groupBy_ function that produce `f (Stream f m a)` groups which are then guaranteed to have the functorial layer.

The `NEStream (Of a) m r` newtype is supporting _sconcat_ which means we can define

```haskell

groupSemigroupBy :: (Semigroup a, Monad m) => (a -> a -> Bool) -> Stream (Of a) m r -> Stream (Of a) m r
groupSemigroupBy f = S.mapped sconcat . groupBy f

```

with expected behavior to collapse groups using semigroup composition

In contrast using the standard _groupBy_ we are stuck with 

```haskell

groupMonoidBy :: (Monoid a, Monad m) => (a -> a -> Bool) -> Stream (Of a) m r -> Stream (Of a) m r
groupMonoidBy f = S.mapped mconcat . groupBy f

```

It would be legit to use an `sconcatUnsafe` that would panic on empty streams because *we know* groups are not empty. 