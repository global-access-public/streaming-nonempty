{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ImportQualifiedPost #-}

import Data.List.NonEmpty hiding (groupBy, map)
import Data.Semigroup (Sum (Sum))
import Streaming (Of, Stream, runIdentity)
import Streaming.NonEmpty (NEStream)
import Streaming.NonEmpty qualified as NES
import Streaming.Prelude qualified as S
import Test.Hspec

import qualified Control.Foldl.NonEmpty as L1

-- | sconcat as in 'Data.Semigroup', with result
sconcat :: (Semigroup a, Monad m) => NEStream (Of a) m r -> m (Of a r)
sconcat = L1.purely NES.fold1 L1.sconcat

-- | what groupBy could be
groupByPure :: (a -> a -> Bool) -> [a] -> [NonEmpty a]
groupByPure equals = runIdentity . S.toList_ . S.mapped NES.toNonEmpty . NES.groupBy equals . S.each

-- | collapse semigroups by some equality
groupSemigroupBy :: (Semigroup a, Monad m) => (a -> a -> Bool) -> Stream (Of a) m r -> Stream (Of a) m r
groupSemigroupBy f = S.mapped sconcat . NES.groupBy f

-- | what should be possible to do with 'groupBy'
-- fmap sconcat . groupBy equals
groupSemigroupByPure :: Semigroup b => (b -> b -> Bool) -> [b] -> [b]
groupSemigroupByPure equals = runIdentity . S.toList_ . groupSemigroupBy equals . S.each

main :: IO ()
main = hspec $ do
  describe "groupSemigroupBy" $ do
    it "collapse no stream" $ do
      xs S.:> () <- S.toList $ groupSemigroupBy (==) $ S.each ([] @(Sum Int))
      xs `shouldBe` []
    it "collapse one elem stream" $ do
      xs S.:> () <- S.toList $ groupSemigroupBy (==) $ S.each [Sum 1]
      xs `shouldBe` [Sum 1]
    it "collapse 2 elems of different groups stream" $ do
      xs S.:> () <- S.toList $ groupSemigroupBy (==) $ S.each [Sum 1, Sum 2]
      xs `shouldBe` [Sum 1, Sum 2]
    it "collapse 2 elems of same group stream " $ do
      xs S.:> () <- S.toList $ groupSemigroupBy (==) $ S.each [Sum 1, Sum 1]
      xs `shouldBe` [Sum 2]
    it "collapse 4 elems of 2 groups stream" $ do
      xs S.:> () <- S.toList $ groupSemigroupBy (==) $ S.each [Sum 1, Sum 1, Sum 2, Sum 2]
      xs `shouldBe` [Sum 2, Sum 4]
  describe "groupSemigroupByPure" $ do
    it "collapse 4 elems of group 2 stream" $ do
      groupSemigroupByPure (==) [Sum 1, Sum 1, Sum 2, Sum 2]
        `shouldBe` [Sum 2, Sum 4]
